const assert = require('assert');
const bcrypt = require('bcrypt');
const mysql = require('mysql');
const util = require('util');

function makeDb(config) {
    const connection = mysql.createConnection(config);
    return {
        query(sql, args) {
            return util.promisify(connection.query)
                .call(connection, sql, args);
        },
        close() {
            return util.promisify(connection.end).call(connection);
        }
    };
}

const db = makeDb({
    host: 'localhost',
    user: 'homestead',
    password: 'secret',
    database: 'DEV_PEM_DCMS'
});

class Account {

    object_id = 0;
    user_uid = '';
    user_name = '';
    user_pass = '';
    digest = '';
    email = '';
    user_confirmed = null;
    user_blocked = null;
    user_sim = null;
    status = 0;
    valid_from = null;
    valid_to = null;
    site_id = 0;

    address_firstname = '';
    address_lastname = '';
    address_company = '';

    static async checkPassword(password, hash) {
        // PHP uses a different bcrypt algorithm than JS, but this should be compatible (in most cases)
        const user_pass = hash.replace('$2y$', '$2b$');
        return util.promisify(bcrypt.compare).call(bcrypt, password, user_pass);
    }

    // This can be anything you need to authenticate a user
    static async authenticate(email, password) {
        try {
            assert(password, 'password must be provided');
            assert(email, 'email must be provided');

            const rows = await db.query("SELECT object_id, user_uid, email, user_pass FROM dcms_access_users WHERE email = ?", String(email).toLowerCase());
            console.log('Rows:', rows[0]);
            const passwordMatches = await Account.checkPassword(password, rows[0].user_pass);
            console.log('Match:', passwordMatches);
            return (passwordMatches ? rows[0].user_uid : undefined);
        } catch (err) {
            console.log(err);
            return undefined;
        }
    }

    // This interface is required by oidc-provider
    static async findAccount(ctx, id) {
        console.log('Called findAccount()', id);
        // This would ideally be just a check whether the account is still in your storage
        const rows = await db.query('SELECT object_id FROM dcms_access_users WHERE user_uid = ?', id);

        if (!rows) {
            return undefined;
        }

        const object_id = rows[0].object_id;

        return {
            accountId: id,
            // and this claims() method would actually query to retrieve the account claims
            async claims() {
                const userRows = await db.query('SELECT au.object_id, au.email, au.user_uid, au.user_confirmed FROM dcms_access_users AS au WHERE au.object_id = ? AND au.user_blocked != 1', object_id);
                const settingRows = await db.query('SELECT setting_key, setting_value FROM dcms_access_user_settings AS au WHERE au.object_id = ?', object_id);

                console.log('UserRows:', userRows);
                console.log('SettingRows:', settingRows);

                const account = new Account();
                account.setProperties(userRows[0]);
                account.setSettings(settingRows)

                console.log('Account:', account);
                return account.toIdToken();
            },
        };
    }

    toIdToken() {
        return {
            sub: this.user_uid,
            email: this.email,
            email_verified: (this.user_confirmed === '1'),
            firstname: this.address_firstname,
            lastname: this.address_lastname,
            company: this.address_company,
        }
    }

    setProperties(properties) {
        for (const propertiesKey in properties) {
            if (this.hasOwnProperty(propertiesKey)) {
                this[propertiesKey] = properties[propertiesKey];
            }
        }
    }

    setSettings(settingsRows) {
        for (const idx in settingsRows) {
            const key = settingsRows[idx].setting_key;
            const val = settingsRows[idx].setting_value;
            if (this.hasOwnProperty(key)) {
                this[key] = val;
            }
        }
    }
}

module.exports = Account;
